# Short Linear Motifs

This text was largely adapted from a [tutorial written by Holger
Dinkel][elm_tutorial] for the [EMBO Practical Course on computational analysis
of protein-protein interactions][embo_course_ppi]

[elm_tutorial]: http://aidanbudd.github.io/course_EMBO_at_TGAC_PPI_Sep2015/trainingMaterial/holgerDinkel/linear_motifs/
[embo_course_ppi]: http://aidanbudd.github.io/course_EMBO_at_TGAC_PPI_Sep2015//

## Eukaryotic Linear Motifs

Eukaryotic Linear Motifs (or ELMS) sometimes also known as short Linear Motifs
(SLiMs) are short sequences typically found in disordered regions that have
important roles in the function of a protein.

## The ELM database

The [ELM database][elm] is a project who's ultimate goal it is to all occurences
of ELMs and their function in all known proteins(!). 

It consists of manually annotated entries carefully curated by experts in a
particular field, working in a certain protein, or a particular motif. These
annotators are responsible for contributing ELM **classes**, which represent
linear motifs with a known function, and experimentally verified **instances**
of this motif.

- **types** There are 6 types of motifs: LIG: ligand binding, MOD:
  modification, TRG: targeting, DOC: docking, DEG: degradation, CLV: cleavage.

- **class** is a sequence of amino acids with a given function, based on
  binding partner, modifying enzyme, acting peptidase and targeted subcellular
  localisation. Each **class** is defined by a **regular expression**

- **instance** an manually annotated occurrence of a **class** in a protein,
  verified by a literature citable experiment.

[elm]: http://elm.eu.org

## Browsing content

## Exercise 1: Browsing content 

There are two main ways in which the ELM database content can be browsed.

Click on "ELM DB" -> "ELM Classes", or follow the link to the ELM classes page:
http://elm.eu.org/elms to browse the ELM **classes** that have been annotated.

Use the search (or side filters) to find the ELM motif: **DOC_CYCLIN_1**

- **Question 1:** What does this motif do? 
- **Question 2:** How many instances are annotated in the database?
- **Question 3:** Which Gene Ontology terms is this motif associated with?  

This motif was identified in P53 in the sequence: **KKLMF**

- **Question 4:** What is the starting and finishing position of this sequence
  in P53?
- **Question 5:** Which experimental protocols were used to infer the existence
  of this instance? 
- **Question 6:** How certain are we about this annotation? 
- **Question 7:** What activates P53 in the pathway to induce apoptosis? 

## Exercise 2: The ELM Prediction tool

Navigate to the "ELM predictions" page.

Search protein **SRC_HUMAN** (accession P12931) for ELMs using the following parameters:

- Cell Compartment: Not specified
- Motif Probability Cutoff: 100
- Context information: (leave blank)

Some questions:

- **Question 1:** How many instances do you find?
- **Question 2:** What can you say about the globularity of the protein? Does
  it have globular and/or disordered regions?

Redo the above search, this time using the following parameters:

- Cell Compartment: cytosol
- Motif Probability Cutoff: 0.01
- Context information: Homo sapiens

Some questions:

- **Question 3:** How many instances do you find now?
- **Question 4:** How many of the instances are manually annotated?
- **Question 5:** Do the structural predictors/filters (SMART, GlobPlot,
  IUPRED, Secondary Structure) agree in terms of which regions are
structured/disordered?
- **Question 6:** Compare the location of the annotated instances with
  structural information at hand (IUPRED, Secondary Structure).
- **Question 7:** How many deteced instances were removed by the
  SMART/Structure filter?
- **Question 8:** For the annotated instances, which of the ELM classes require
  a phosphorylation at a certain residue of the motif? (Hint: This information
can be found in the description of the ELM class)
- **Question 9:** Which residue in SRC_HUMAN corresponds to this and can you
  find evidence for a phosphorylation of this residue (using Phospho.ELM)?


## Exercise 3: The ELM Prediction tool

Search ELM using the protein name **MDM4_HUMAN** and look for the ‘USP binding motif’ **DOC_USP7_MATH_1**

- **Question 1:** How many such motif instances are found in this protein sequence?
- **Question 2:** How many of these have been exprimentally validated (i.e., are manually annotated?), and what are the "FP" annotations?

## Exercise 4: Switches 

Use the ELM "global search box" (on the top right) to search for the class
**LIG_SH3_2**. (Just start typing, and wait for the autocomplete to finish).

Click on "LIG_SH3_2" to visit the class page.

- **Question 1:** How many switches are annotated for this class?
- **Question 2:** What is the mechanism that results in the switching event in **SYNJ2_RAT**?
