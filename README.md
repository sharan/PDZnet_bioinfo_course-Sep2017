## 1st network progress meeting in EMBL

### Training Course: Bioinformatics tools to study PPI (September 26 - September 29)

| Day | Time          | Plan                                                                                                                                                                             |
|-----|---------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | 09:00 - 09:30 | Coffee/Introduction to the course                                                                                                                                                |
| 1   | 09:30 - 11:00 | Practical session/Marc Gouw, Malvika Sharan, Jelena Calyseva/Linux commandline course - I                                                                                        |
| 1   | 11:00 - 11:30 | Coffee break                                                                                                                                                                     |
| 1   | 11:30 - 12:30 | Practical session/Malvika Sharan, Marc Gouw, Jelena Calyseva/Linux commandline course - II                                                                                       |
| 1   | 12:30 - 14:00 | Lunch                                                                                                                                                                            |
| 1   | 14:00 - 15:30 | Practical session and demonstration/Jelena Calyseva/Protein databases and related annotation resources                                                                           |
| 1   | 15:30 - 16:00 | Coffee break                                                                                                                                                                     |
| 1   | 16:00 - 17:00 | Practical session and demonstration/Malvika Sharan/Studying similar sequences (BLAST, MSA etc.) and EBI-EMBOSS toolkits                                                          |
| 1   | 17:00         | End                                                                                                                                                                              |
| 2   | 09:30 - 10:30 | Lecture/Toby Gibson/Complexity of PPIs                                                                                                                                           |
| 2   | 10:00 - 10:30 | Coffee break                                                                                                                                                                     |
| 2   | 10:30 - 11:30 | Practical session and demonstration/Jelena Calyseva, Malvika Sharan/Tools and resources for the analysis of the structural architecture of a protein (PDB, InterPro, TMHMM etc.) |
| 2   | 11:30 - 12:30 | Practical activity and demonstration/Marc Gouw, Malvika Sharan/Understanding linear motifs & intrinsically disordered region-1 (ELM, SLiMSearch, ProViz)                         |
| 2   | 12:30 - 13:30 | Lunch break                                                                                                                                                                      |
| 2   | 13:30 - 15:00 | Practical activity and demonstration/Marc Gouw, Malvika Sharan/Understanding linear motifs & intrinsically disordered region-2 (JPred, IUPRED, Anchor)                           |
| 2   | 15:00 - 15:30 | Coffee break                                                                                                                                                                     |
| 2   | 15:30 - 17:00 | Practical activity/Toby Gibson/Revealing interactive features in protein multiple sequence alignments with Jalview                                                               |
| 2   | 17:00         | end                                                                                                                                                                              |
| 3   | 09:00 - 11:00 | Practical activity and demonstration/Matt Rogon/Network analysis using Cytoscape - I                                                                                             |
| 3   | 11:00 - 11:30 | Coffee break                                                                                                                                                                     |
| 3   | 11:30 - 12:30 | Practical session and demonstration/Toby Gibson/Visualizing protein structures and interaction interfaces with UCSF Chimera                                                      |
| 3   | 12:30 - 13:30 | Lunch                                                                                                                                                                            |
| 3   | 13:30 - 15:00 | Practical activity and demonstration/Jelena Calyseva/Sequence/structure alignment based study of PDZ domains                                                                     |                                                                                                  |
| 3   | 15:00 – 15:30 | Coffee                                                                                                                                                                           |
| 3   | 15:30 - 17:00 | Practical session and demonstration/Michael Kuhn/Protein association networks with STRING                                                                                        |
| 3   | 17:00         | end                                                                                                                                                                              |
| 4   | 09:00 - 10:30 | Practical activity and demonstration/Matt Rogon/Network analysis using Cytoscape to integrate experimental datasets (e.g. BIO-ID) 1                                              |
| 4   | 10:30 - 11:00 | Coffee break                                                                                                                                                                     |
| 4   | 11:00 - 12:30 | Practical activity and demonstration/Matt Rogon/Network analysis using Cytoscape to integrate experimental datasets (e.g. BIO-ID) 2                                              |
| 4   | 12:30 – 13:30 | Lunch                                                                                                                                                                            |
| 4   | 13:30 – 15:00 | Practical activity/Group tasks: Work on your protein(s) of interest and prepare group presentations                                                                              |
| 4   | 15:00 – 15:30 | Coffee                                                                                                                                                                           |
| 4   | 15:30 - 17:00 | Practical activity/Group tasks: Group presentations and final feedback                                                                                                           |
| 4   | 17:00         | end                                                                                                                                                                              |